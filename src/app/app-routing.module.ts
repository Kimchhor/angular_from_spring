import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ViewPageComponent} from "./core/component/view-page/view-page.component";
import {UpdatePageComponent} from "./core/component/update-page/update-page.component";
import {MainPageComponent} from "./main-page/main-page.component";
import {AddNewComponent} from "./core/component/add-new/add-new.component";
import {PageNotFoundComponent} from "./core/component/page-not-found/page-not-found.component";

const routes : Routes = [
  {path:'', component:MainPageComponent},
  {path:'view/:viewId', component:ViewPageComponent},
  {path:'update/:updateId', component:UpdatePageComponent},
  {path:'new', component:AddNewComponent},
  {path:'**',component: PageNotFoundComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],exports:[
    RouterModule
  ],
})
export class AppRoutingModule { }
