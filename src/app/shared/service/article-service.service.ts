import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {IArticle} from "../../model/article";
import {catchError, switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ArticleServiceService {

  // baseUrl = 'https://jsonplaceholder.typicode.com/posts';
  //   baseUrl = 'http://localhost:8080/api/v1/article/';
  baseUrl = 'http://110.74.194.124:3034/api/articles';

  constructor(private http: HttpClient) {
  }

  // getAllPost():Observable<any>{
  //   return this.http.get<any>(this.baseUrl);
  // }
  //===================== fetch all ========================================
  getAllArticle(): Observable<any> {
    return this.http.get<any[]>(this.baseUrl);
  }

  //===================== fetch by id ======================================
  getArticleById(postId: String): Observable<any> {
    return this.http.get<any>(this.baseUrl + "/" + postId);
  }

  //====================== post =============================================
  postArticle(article: IArticle, file: any) {
    console.log(file)
    if (file) {
      console.log(true);
      const formData: FormData = new FormData();
      formData.append('image', file);
      return this.http.post<any>("http://110.74.194.124:3034/api/images", formData
      ).pipe(
        switchMap(
          (value) => this.http.post<any>(this.baseUrl, {
            "title": article.title,
            "description": article.description,
            "published": true,
            "image": value.url
          })
        )
      )
    } else {
      console.log(false);
      return this.http.post(this.baseUrl, {
        "title": article.title,
        "description": article.description,
        "published": true,
        "image": article.image
      }).pipe(
        catchError(this.handleError)
      );
    }


  }

  //======================== delete ================================
  deleteArticleById(articleId: String): Observable<any> {
    return this.http.delete(this.baseUrl + "/" + articleId);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

//  ================ edit ===============================
  updateArticle(article: IArticle, id: String, file: any): Observable<any> {
    console.log(file)
    if (file) {
      console.log(true);
      const formData: FormData = new FormData();
      formData.append('image', file);
      return this.http.post<any>("http://110.74.194.124:3034/api/images", formData
      ).pipe(
        switchMap(
          (value) => this.http.patch<any>(this.baseUrl+ "/" + id, {
            "title": article.title,
            "description": article.description,
            "published": true,
            "image": value.url
          })
        )
      )
    } else {
      // console.log("id for update",id)
      // console.log("data for update", article.title)
      console.log(false);
      return this.http.patch(this.baseUrl + "/" + id,
        {
          "title": article.title,
          "description": article.description,
          "published": true,
          "image": article.image
        }
      ).pipe(
        catchError(this.handleError)
      );
    }


  }
}
