import { Component, OnInit } from '@angular/core';
import {ArticleServiceService} from "../../../shared/service/article-service.service";
import {IArticle} from "../../../model/article";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {every} from "rxjs/operators";
import Swal from "sweetalert2";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css']
})
export class AddNewComponent implements OnInit {
  // inputArticle : FormGroup
  inputArticle : FormGroup ;
  constructor(private service: ArticleServiceService, private formBuilder : FormBuilder, private route:Router) {
    this.inputArticle = this.formBuilder.group({

      title :new FormControl('',[Validators.required]),
      description :new FormControl('',[Validators.required]),
      img :new FormControl('')
    })
  }
file!: File;
  article1 : IArticle = {
    _id: "",
    title: "Hello world",
    description: "168168168168168168168168168168168168168",
    published:true,
    image: "string",
  }
  article !: IArticle ;
  obj !:any;

  // onAddArticle(){
  //   let article : IArticle = {
  //     _id: "",
  //     title: "Hello world",
  //     description: "168168168168168168168168168168168168168",
  //     image: ""
  //   }
  //   this.service.postArticle(this.article).subscribe(data=>{
  //     console.log(data)
  //   })
  // }

  ngOnInit(): void {

  }
url = "../assets/people.jfif"
onSelectFile(e:any){
    if (e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event:any)=>{
        this.url = event.target.result;
      }
      this.file = e.target.files[0];
    }
}

get title(){
    return this.inputArticle.get("title")
}
get description(){
    return this.inputArticle.get("description")
}
get img(){
    return this.inputArticle.get("img");
}

  onSubmit(){
    this.article = this.inputArticle.value;
    this.article.image = 'www.google.com';
    // console.log(this.article)
    this.postData();
  }

  postData() {
    this.service.postArticle(this.article, this.file).subscribe(
      x=>{
        // console.log(x)
      //  ------------- show message --------------------
      this.alertWithSuccess(x.message);
      //---------- redirect to homepage------------------
      this.route.navigateByUrl('').then(r => {
        });
      }
    );
  }

  alertWithSuccess(message:any){
    Swal.fire('Thank you...', message, 'success')
  }
}
