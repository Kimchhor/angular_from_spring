import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ArticleServiceService} from "../../../shared/service/article-service.service";
import {ActivatedRoute} from "@angular/router";
import {File} from "@angular/compiler-cli/src/ngtsc/file_system/testing/src/mock_file_system";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-page',
  templateUrl: './update-page.component.html',
  styleUrls: ['./update-page.component.css']
})
export class UpdatePageComponent implements OnInit {
  id!: String;
  post!: any;
  updateArticle !: FormGroup ;
  constructor(private service: ArticleServiceService, private formBuilder : FormBuilder, private route :ActivatedRoute) {
    this.updateArticle = this.formBuilder.group({

      title :new FormControl('',[Validators.required]),
      description :new FormControl('',[Validators.required]),
      img :new FormControl('')
    })
  }
file !: File;
  url = "../assets/people.jfif"
  onSelectFile(e:any){
    if (e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event:any)=>{
        this.url = event.target.result;
      }
      this.file = e.target.files[0];
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.updateId;
    });
    this.service.getArticleById(this.id).subscribe(
      val=>{
        this.post = val.data;
        this.url = this.post.image;
        // console.log("post",this.post);
        this.onUpdate(this.post);
      }
    );
  }
  get title(){
    return this.updateArticle.get("title")
  }
  get description(){
    return this.updateArticle.get("description")
  }
  get img(){
    return this.updateArticle.get("img");
  }

  onUpdate(data: any) {
    console.log("data for update", data);
    this.updateArticle.get('title'
    )?.setValue(data.title);
    this.updateArticle.get('description'
    )?.setValue(data.description);
  }

  updateData(){
    this.post = this.updateArticle.value;
    this.service.updateArticle(this.post, this.id, this.file).subscribe(
      val =>{
        // console.log(val.message);
        this.alertWithSuccess(val.message);
      }
    )
  }
  alertWithSuccess(message:any){
    Swal.fire('Thank you...', message, 'info')
  }
}
