export interface IArticle{
  _id: String;
  title: String;
  description: String;
  published: boolean;
  image :String;
}
